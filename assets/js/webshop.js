$(document).ready(function () {

    //Custom validator za proveru email adrese
    jQuery.validator.addMethod("validEmail", function(value, element)
    {
        if(value == '')
            return true;
        var temp1;
        temp1 = true;
        var ind = value.indexOf('@');
        var str2=value.substr(ind+1);
        var str3=str2.substr(0,str2.indexOf('.'));
        if(str3.lastIndexOf('-')==(str3.length-1)||(str3.indexOf('-')!=str3.lastIndexOf('-')))
            return false;
        var str1=value.substr(0,ind);
        if((str1.lastIndexOf('_')==(str1.length-1))||(str1.lastIndexOf('.')==(str1.length-1))||(str1.lastIndexOf('-')==(str1.length-1)))
            return false;
        str = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/;
        temp1 = str.test(value);
        return temp1;
    }, "Neispravan format email adrese.");

    /* Validacija forme za registraciju */
    $("#registracija").validate({
        rules: {
            korisnickoIme: "required",
            email: {
                required: true,
                validEmail: true
            },
            sifra: {
                required: true,
                minlength: 6
            },
            adresa: "required",
            mobilni: {
                required: true
            }
        },
        messages: {
            korisnickoIme: "Korisničko ime je obavezno polje.",
            email: {
                required: "Email adresa je obavezno polje.",
                validEmail: "Neispravan format email adrese."
            },
            sifra: {
                required: "Šifra je obavezno polje.",
                minlength: "Minimalna dužina šifre je 6 karaktera."
            },
            adresa: "Adresa je obavezno polje.",
            mobilni: {
                required: "Mobilni je obavezno polje."
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error')
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success')
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    /* Validacija forme za update profila */
    $("#profil").validate({
        rules: {
            email: {
                required: true,
                validEmail: true
            },
            adresa: "required",
            mobilni: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Email adresa je obavezno polje.",
                validEmail: "Neispravan format email adrese."
            },
            adresa: "Adresa je obavezno polje.",
            mobilni: {
                required: "Mobilni je obavezno polje."
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error')
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success')
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    /* Validacija forme za logovanje */
    $("#logovanje").validate({
        rules: {
            korisnickoIme: "required",
            sifra: {
                required: true,
            }
        },
        messages: {
            korisnickoIme: "Korisničko ime je obavezno polje.",
            sifra: {
                required: "Šifra je obavezno polje.",
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error')
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success')
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    /* Validacija forme za unos novih proizvoda */
    $("#novi_proizvod").validate({
        rules: {
            glavnaKategorija: "required",
            Kategorija: "required",
            Naziv: "required",
            Kratakopis: "required",
            Dugiopis: "required",
            Cena: {
                required: true,
                number: true
            },
            Slika: {
                required: true,
                extension: "jpg|gif|png|jpeg"
            }
        },
        messages: {
            glavnaKategorija: "Morate izabrati kategoriju.",
            Kategorija: "Morate izabrati kategoriju.",
            Naziv: "Naziv je obavezno polje..",
            Kratakopis: "Kratak opis je obavezno polje.",
            Dugiopis: "Dugiopis je obavezno polje.",
            Cena: {
                required: "Cena je obavezno polje.",
                number: "Dozvoljeni su samo brojevi."
            },
            Slika: {
                required: "Slika je obavezno polje.",
                extension: "Dozvoljene su samo slike (jpg, gif, png, jpeg)."
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error')
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success')
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    /* Ajax poziv za popunjavanje select polja sa potkategorijama */
    $('select[name=glavnaKategorija]').on('change', function () {

        var imekategorije = $('select[name=glavnaKategorija]').val();

        $.ajax({
            url: './include/skripte/ajaxPodkategorije.php',
            type: 'POST',
            data: {imekategorije: imekategorije},
            async: false,
            success: function (response) {
                var podkategorije = JSON.parse(response);
                var options = podkategorije == "" ? "<option selected></option>" : "";
                for (var i = 0; i < podkategorije.length; i++) {
                    options += '<option value="' + podkategorije[i]["IDKategorije"] + '">' + podkategorije[i]["Ime"] + '</option>';
                }
                $('select[name=Kategorija]').html(options);
            },
            error: function () {
            }
        })
    });

    /* Modalni porozor za prikaz proizvoda */
    $('.detaljnije').on('click', function () {
        var proizvodid = this.getAttribute('data-id');

        $.ajax({
            url: './include/skripte/ajaxProizvodModal.php',
            type: 'POST',
            data: {proizvodid: proizvodid},
            async: false,
            success: function (response) {
                var proizvod = JSON.parse(response);
                $('#proizvodModalNaslov').html(proizvod.Naziv);
                $('#proizvodModalSlika').attr('src', "./slike_proizvodi/" + proizvod.Slika);
                $('#proizvodModalOpis').text(proizvod.Dugiopis);
                $('#proizvodModalCena').text(proizvod.Cena);
                $('#proizvodModalId').val(proizvod.IDProizvoda);
            },
            error: function () {
            }
        })
    });

    /* Ajax poziv za cuvanje proizvoda u korpu */
    $('#uKorpu').on('click', function () {
        var proizvodid = $('#proizvodModalId').val();
        var kolicina = $('#proizvodModalKolicina').val();
        var naziv = $('#proizvodModalNaslov').html();
        var cena = $('#proizvodModalCena').text();

        $.ajax({
            url: './include/skripte/ajaxKorpa.php',
            type: 'POST',
            data: {proizvodid: proizvodid, kolicina: kolicina, naziv: naziv, cena: cena},
            async: false,
            success: function (brojProizvoda) {
                $('#korpaBroj').text(brojProizvoda);
                $('#proizvodModal').modal('hide');
            },
            error: function () {
            }
        })
    });

    //Svidja/Ne svidja mi se proizvod
    $('button.like').on('click', function () {
        var button = $(this);
        var tip = button.data('tip');
        var proizvodid = button.data('proizvodid');
        var korisnik = button.data('korisnik');
        var brojsvidjanja = button.data('brojsvidjanja');

        if (Cookies.get("PID" + proizvodid) == undefined || Cookies.get("PID" + proizvodid) != korisnik) {
            $.ajax({
                url: './include/skripte/ajaxGlasajProizvod.php',
                type: 'POST',
                data: {tip: tip, proizvodid: proizvodid},
                async: false,
                success: function (response) {
                    Cookies.set("PID" + proizvodid, korisnik);
                    if (tip == "gore") {
                        button.html('<span class="glyphicon glyphicon-thumbs-up"> (' + (brojsvidjanja + 1) + ')</span>');
                    } else {
                        button.html('<span class="glyphicon glyphicon-thumbs-down"> (' + (brojsvidjanja + 1) + ')</span>');
                    }
                },
                error: function () {
                }
            })
        }
    })
});

/* Funkcija za izbacivanje proizvoda iz korpe */
function izbaciProizvod(id) {
    $.ajax({
        url: './include/skripte/ajaxIzbaciProizvodKorpa.php',
        type: 'POST',
        data: {id: id},
        async: false,
        success: function (response) {
            if (response)
                window.location.replace('./?strana=korpa');
        },
        error: function () {
        }
    })
}

/* Funkcija za praznjenje celokupne korpe */
function isprazniKorpu() {
    $.ajax({
        url: './include/skripte/ajaxIsprazniKorpu.php',
        type: 'POST',
        async: false,
        success: function (response) {
            if (response)
                window.location.replace('./');
        },
        error: function () {
        }
    })
}

/* Funkcija za slanje porudzbine */
function posaljiPorudzbinu() {
    $.ajax({
        url: './include/skripte/ajaxPosaljiPorudzbinu.php',
        type: 'POST',
        async: false,
        success: function (response) {
            if (response)
                window.location.replace('./');
        },
        error: function () {
        }
    })
}

/* Funkcija za brisanje/isporuku porudzbine -administratorski deo*/
function administratorPorudzbine(idPorudzbine, isporuci) {
    $.ajax({
        url: './include/skripte/ajaxAdministratorPorudzbine.php',
        type: 'POST',
        data: {idPorudzbine: idPorudzbine, isporuci: isporuci},
        async: false,
        success: function (response) {
            if (response)
                window.location.replace('./?strana=porudzbine');
            else
                window.location.replace('./');
        },
        error: function () {
        }
    })
}
