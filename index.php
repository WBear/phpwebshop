<?php
session_start();

include('./confPromenljive.php');
include('./include/skripte/dbKonekcija.php');
include('./include/skripte/funkcije.php');

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WebShop</title>

    <!-- Uvoz css fajlova -->
    <link rel="stylesheet" type="text/css" href="./assets/bootstrap-3.3.6-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="./assets/css/webshop.css"/>

    <!-- Uvoz javascript fajlova -->
    <script src="./assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="./assets/bootstrap-3.3.6-dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="./assets/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="./assets/js/js.cookie.js" type="text/javascript"></script>
    <script src="./assets/js/webshop.js" type="text/javascript"></script>
<body>
<div class="container-fluid">

    <!-- Header -->
    <?php include("./include/layout/header.php"); ?>

    <!-- Sadrzaj -->
    <div class="col-md-12">
        <div class="row flex">
            <!-- Dinamicki sadrzaj -->
            <div class="col-md-9 sadrzaj">
                <?php
                //Provera dali je setovana promenljiva "strana" u URL i dali se vrednost nalazi na listi postojecih strana.
                //U suprotnom ucitaj naslovnu stranu.
                if (isset($_GET['strana']) && in_array($_GET['strana'], $LISTA_STRANA))
                    include('./include/strane/' . $_GET['strana'] . '.php');
                else
                    include('./include/strane/naslovna.php');
                ?>
            </div>
            <!-- Sidebar -->
            <div class="col-md-3 sidebar">
                <?php include("./include/layout/sidebar.php"); ?>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <div class="col-md-12">
        <div class="row">
            <footer class="col-md-12 text-center">
                Internet prodavnica d.o.o, &copy; 2016.
            </footer>
        </div>
    </div>
</div>
<?php include("./include/layout/proizvodModal.php"); ?>
</body>
</html>