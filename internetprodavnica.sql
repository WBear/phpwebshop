-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 13, 2016 at 02:22 AM
-- Server version: 5.5.45-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `internetprodavnica`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategorije`
--

CREATE TABLE IF NOT EXISTS `kategorije` (
  `IDKategorije` int(9) NOT NULL AUTO_INCREMENT,
  `Ime` varchar(150) NOT NULL,
  `Podkategorija` int(9) DEFAULT NULL,
  PRIMARY KEY (`IDKategorije`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `kategorije`
--

INSERT INTO `kategorije` (`IDKategorije`, `Ime`, `Podkategorija`) VALUES
(1, 'Odeća', NULL),
(2, 'Knjige', NULL),
(3, 'Elektronika', NULL),
(4, 'Ženska', 1),
(5, 'Muška', 1),
(6, 'Dečija', 1),
(7, 'Fikcija', 2),
(8, 'Biografije', 2),
(9, 'Politika', 2),
(10, 'Istorija', 2),
(11, 'Mobilni telefoni', 3),
(12, 'Laptopovi', 3),
(13, 'Računari', 3);

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE IF NOT EXISTS `korisnici` (
  `IDKorisnika` int(9) NOT NULL AUTO_INCREMENT,
  `Ime` varchar(150) DEFAULT NULL,
  `Prezime` varchar(150) DEFAULT NULL,
  `Email` varchar(150) NOT NULL,
  `korisnickoIme` varchar(45) NOT NULL,
  `Sifra` varchar(45) NOT NULL,
  `Mobilni` varchar(45) NOT NULL,
  `Adresa` varchar(150) NOT NULL,
  `Komentar` text,
  `Kolica` text,
  `administrator` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IDKorisnika`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000010 ;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`IDKorisnika`, `Ime`, `Prezime`, `Email`, `korisnickoIme`, `Sifra`, `Mobilni`, `Adresa`, `Komentar`, `Kolica`, `administrator`) VALUES
(100000002, 'Petar', 'Petrovic', 'test2@test.com', 'test2', '123123', '064-123132', 'Makedonska 1a, Beograd', NULL, '', 0),
(100000006, NULL, NULL, 'maki@maki.com', 'test', '123123', '123-123123', 'moja adresa', NULL, NULL, 1),
(100000007, 'Fikreta', 'Alomerović', 'etf84ita@gmail.com', 'fiki', '123456', '061-1113213', 'Uciteljska 44', NULL, '', 0),
(100000008, NULL, NULL, 'marko@test.com', 'Marko', '123456', '060-111222', 'Nemanjina 10', NULL, '{"100000026":{"kolicina":"1","naziv":"Skitalac - Dejvid Gemel","cena":"790"}}', 0),
(100000009, 'Milan', 'Milanović', 'milan@test.com', 'Milan', '123456', '065-1234567', 'Nehruova 21', NULL, '[]', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kupovine`
--

CREATE TABLE IF NOT EXISTS `kupovine` (
  `IDKupovine` int(9) NOT NULL AUTO_INCREMENT,
  `Korisnik` int(9) NOT NULL,
  `Kupljeniproizvodi` text NOT NULL,
  `Isporuceno` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`IDKupovine`),
  KEY `Korisnik` (`Korisnik`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000005 ;

--
-- Dumping data for table `kupovine`
--

INSERT INTO `kupovine` (`IDKupovine`, `Korisnik`, `Kupljeniproizvodi`, `Isporuceno`) VALUES
(100000001, 100000002, '{"100000007":{"kolicina":"1","naziv":"Svečana haljina Maggie Sottero","cena":"8500"},"100000001":{"kolicina":"1","naziv":"Karirana košulja","cena":"2000"},"100000005":{"kolicina":"2","naziv":"Sveobuhvatna enciklopedija mitologije","cena":"3900"},"korisnik":{"imePrezime":"test2 test2","adresa":"test2 test2 22","mobilni":"068\\/"}}', '2016-06-08 11:01:19'),
(100000002, 100000002, '{"100000007":{"kolicina":"1","naziv":"Svečana haljina Maggie Sottero","cena":"8500"},"100000012":{"kolicina":"1","naziv":"Haljina","cena":"2500"},"100000016":{"kolicina":"2","naziv":"Kaput","cena":"5000"},"100000008":{"kolicina":"2","naziv":"Mu&scaron;ko odelo ","cena":"10000"},"100000006":{"kolicina":"2","naziv":"AUTOSTOPERSKI VODIČ KROZ GALAKSIJU","cena":"990"},"korisnik":{"imePrezime":"Petar Petrović","adresa":"Makedonska 1a, Beograd","mobilni":"064-123132"}}', NULL),
(100000003, 100000002, '{"100000007":{"kolicina":3,"naziv":"Sve\\u010dana haljina Maggie Sottero","cena":"8500"},"100000012":{"kolicina":"1","naziv":"Haljina","cena":"2500"},"100000018":{"kolicina":"1","naziv":"Stiven King - Dalas 63","cena":"1390"},"korisnik":{"imePrezime":"Petar Petrovic","adresa":"Makedonska 1a, Beograd","mobilni":"064-123132"}}', NULL),
(100000004, 100000002, '{"100000003":{"kolicina":"3","naziv":"Nenadma\\u0161ni genije - \\u017divot Nikole Tesle","cena":"1210"},"100000027":{"kolicina":"1","naziv":"Mikelan\\u0111elo","cena":"990"},"korisnik":{"imePrezime":"Petar Petrovic","adresa":"Makedonska 1a, Beograd","mobilni":"064-123132"}}', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `proizvodi`
--

CREATE TABLE IF NOT EXISTS `proizvodi` (
  `IDProizvoda` int(9) NOT NULL AUTO_INCREMENT,
  `Kategorija` int(9) NOT NULL,
  `Naziv` varchar(150) NOT NULL,
  `Slika` varchar(250) NOT NULL,
  `Kratakopis` varchar(150) DEFAULT NULL,
  `Dugiopis` text,
  `Cena` int(11) NOT NULL,
  `BrojSvidjanja` int(11) DEFAULT '0',
  `BrojNesvidjanja` int(11) DEFAULT '0',
  PRIMARY KEY (`IDProizvoda`),
  KEY `Kategorija` (`Kategorija`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000032 ;

--
-- Dumping data for table `proizvodi`
--

INSERT INTO `proizvodi` (`IDProizvoda`, `Kategorija`, `Naziv`, `Slika`, `Kratakopis`, `Dugiopis`, `Cena`, `BrojSvidjanja`, `BrojNesvidjanja`) VALUES
(100000001, 6, 'Karirana košulja', 'DSC_7892.JPG', 'Dečija košulja, dugi rukav, karirana...', 'teeeeest', 2000, 16, 0),
(100000002, 6, 'Ženski komplet', 'DSC_7904.JPG', 'Komplet džemper i suknja za devojčice...', 'bla bla bla', 3000, 14, 0),
(100000003, 8, 'Nenadmašni genije - Život Nikole Tesle', 'tesla.jpg', 'Knjiga o životu jednog od najvećih svetskih umova', 'Nikola Tesla je jedan od najoriginalnijih mislilaca u istoriji ljudskog roda. Ne postoji nijedno područje tehničke civilizacije na kojem on nije radio kao preteča ili na kojem njegov rad nije ubrzao rad ostalih naučnika, pa se sa razlogom smatra stubom civilizacije. Biografi i obožavaoci Aristotela kažu da je civilizacija upravo onda počela kada se rodio ovaj veliki filozof. Nešto slično tome može se slobodno reći i za Teslu, jer je u savremenoj civilizaciji napravljen džinovski korak tek onda kada je Tesla dao rešenje problema obrtnog magnetnog polja i polifaznog sistema naizmeničnih struja i time omogućio prenos električne energije na velike daljine. Imajući u vidu da je knjiga Džona O''Nila do sada služila kao najviše citirana u spisku literature mnogim domaćim i stranim autorima, ona, bez svake sumnje, predstavlja "pravi traktat" za mnoge čitaoce koji vole i poštuju Teslu i njegovo istinsko delo.', 1210, 9, 1),
(100000004, 9, 'Noam Čomski - Imperijalne ambicije', 'imperijalne_ambicije.jpg', 'Razgovori o svetu posle 11-tog septembra. Ako ste čitali knjigu "Propaganda i javno mnjenje" pročitajte i ovaj njen nastavak.', 'Razgovori o svetu posle 11-tog septembra. Ako ste čitali knjigu &quot;Propaganda i javno mnjenje&quot; pročitajte i ovaj njen nastavak. Ako hoćete nešto da učinite, morate u to čvrsto da verujete i da se tome neprestano posvećujete, iz dana u dan, da sprovodite programe prosvećivanja, da se organizujete, aktivirate. Tako stvari mogu da se menjaju. Ako želite magični štapić kojim čete zamahnuti, pa će se vratiti na staro i opet provoditi vreme gledajućiteleviziju, znajte - to ne postoji.', 700, 3, 2),
(100000005, 10, 'Sveobuhvatna enciklopedija mitologije', 'knjiga-sveobuhvatna-enciklopedija-mitologije.jpg', 'Enciklopedija mitologije sa preko hiljadu ilustracija', 'Sveobuhvatni prikaz zadivljujućih priča i legendi najfascinantnijih i najočaravajućih mitologija drevnog sveta &uml; Od A do š kada su u pitanju mitološke pojave dva velika kontinenta &uml; Preko hiljadu abecednih unosa opisuje središnje mitološke figure svake kulture ponaosob, njihovu važnost za drevne civilizacije &uml; Prelepo ilustrovano, sa više od hiljadu slika koje sežu preko petnaest vekova, od drevnih vremena do sadašnjih dana, i prikazuju nam najlepšu umetnost, uključujući i posebno naručena umetnička dela &uml; Izvanredan slikoviti prikaz mitoloških tema i simbola od ključne važnosti svakoj od navedenih kultura &uml; Dva najveća stručnjaka u svojim poljima pružaju nam životnu pripovest velikog naučnog značaja &uml; Ovo je jedna bezvremena knjiga - pravi klasik za sva vremena', 3900, 7, 2),
(100000006, 7, 'AUTOSTOPERSKI VODIČ KROZ GALAKSIJU', 'knjiga-autostoperski-vodic-kroz-galaksiju.jpg', 'Jedna od najpopularnijih i najčitanijih SF knjiga svih vremena!', 'Kompletna ''autostoperska'' serija u jednoj knjizi: Autostoperski vodič kroz galaksiju; Restoran na kraju vaseljene; Život, vaseljena i sve ostalo; Do viđenja i hvala na svim ribama; Uglavnom bezopasni. Iako je nastao davne 1977. godine, prvenstveno kao radio-serijal, Autostoperski vodič kroz galaksiju ne prestaje da oduševljava svojim britkim, intergalaktičkim humorom, nezaboravnim dosetkama i jedinstvenim likovima, koji prolaze kroz najneverovatnije avanture. Kao jedan od najblistavijih primera svojevrsnog parodiranja žanra, ovaj roman postavio je nove, nezaobilazne standarde prema kojima se i dan-danas ravnaju kako čitaoci, tako i drugi pisci. Daglas Adams (1952&ndash;2001) nenadmašni, višestruko talentovani genije, jedan od najvećih humorista našeg vremena, tehnofil sofisticiranog humora koji je ostavio neizbrisiv trag u baštini svetske kulture. Knjiga koja je prodata u nekoliko desetina miliona primeraka!', 990, 10, 0),
(100000007, 4, 'Svečana haljina Maggie Sottero', 'haljina roze.jpg', 'Svečana haljina izuzetnog izgleda i kvaliteta', 'Svečane haljine Maggie Sottero su dizajnirane za devojke koje zele da budu primećene, glamurozni vezeni materijali i unikatni krojevi ispuniće sva vaša očekivanja. Haljine koje nose poznate ličnosti kako u svetu tako i kod nas. Svečana haljina koja u pravom smislu te reči vaš svečani trenutak čini nezaboravnim.', 8500, 15, 1),
(100000008, 5, 'Muško odelo ', 'Musko odelo.png', 'Muško odelo za svaku priliku', 'Elegantno odelo marke Daniel Ladrup, kvalitetne izrade i modernog kroja, za svaku priliku. Dostupno u veličinama do 58.', 10000, 1, 6),
(100000009, 5, 'Majica ', 'katalog_b2b_16302_obrazek_zoom.jpg', 'Majica kratkih rukava ', 'Materijal: 65% POLYESTER, 35% COTTON\r\nBoja: RODODENDRON MELANGE\r\nVeličina: S,M,L,XL,XXL,XXXL', 1500, 0, 0),
(100000010, 5, 'Muška majica', 'DSCI5363-web1.jpg', 'LIMITATIONS MUŠKA CLASSIC MAJICA SIVA', 'Boja: siva\r\nDostupno i u teget boji.\r\n\r\nKolekcija: proleće leto 2015\r\n\r\nSirovinski sastav: 100% pamuk', 1560, 0, 0),
(100000011, 4, 'Ženska košulja, jednobojna, dva džepa sa dekorativnim dugmićima', 'katalog_b2b_15864_obrazek_zoom.jpg', 'Košulja ', ' 100% Pamuk\r\n\r\nMaterijal: 100% Pamuk\r\nBoja: NAVY\r\nVeličina: XS,S,M,L,XL,XXL', 1200, 2, 0),
(100000012, 4, 'Haljina', 'katalog_b2b_15832_obrazek_zoom.jpg', 'Letnja haljina', '\r\nMaterijal: 65% Poliester, 35% Pamuk\r\nBoja: Roze\r\nVeličina: XS,S,M,L,XL,XXL', 2500, 2, 0),
(100000013, 4, 'Haljina', 'katalog_b2b_15900_obrazek_zoom.jpg', 'Letnja haljina', 'Materijal: 95% Viskoza, 5% Elastin\r\nBoja: Crna\r\nVeličina: XS,S,M,L,XL,XXL', 3000, 1, 0),
(100000014, 4, 'Haljina', 'katalog_b2b_15937_obrazek_zoom.jpg', 'Letnja haljina sa V izrazom', ' 100% Viscose\r\n\r\n\r\nMaterijal: 100% Viskoza\r\nBoja: Roze-plava\r\nVeličina: 34,36,38,40,42,44', 3000, 1, 0),
(100000015, 4, 'Majica', 'katalog_b2b_15907_obrazek_zoom.jpg', 'Majica kratkih rukava', '\r\nMaterijal: 60% Pamuk, 40% Poliester\r\nBoja: Siva\r\nVeličina: XS,S,M,L,XL,XXL', 2000, 1, 0),
(100000016, 4, 'Kaput', 'katalog_b2b_15741_obrazek_zoom.jpg', 'Kaput deblji', 'Materijal: 100% Poliester\r\nBoja: Pesak\r\nVeličina: XS,S,M,L,XL,XXL', 5000, 1, 0),
(100000017, 4, 'Farmerke', 'katalog_b2b_15690_obrazek_zoom.jpg', 'Letnej farmerke', ' 100% Tencel\r\n\r\nMaterijal: 100% TENCEL\r\nBoja: SUMMER DENIM\r\nVeličina: 25,26,27,28,29,30,31,32,33/26&quot;,28&quot;\r\n', 2500, 1, 1),
(100000018, 7, 'Stiven King - Dalas 63', 'dalas.jpg', 'Svetski bestseler broj jedan', 'Dana 22. novembra 1963. godine tri pucnja odjeknula su u Dalasu. Džon Kenedi je ubijen i svet je zauvek izmenjen. šta bi bilo kada bi to moglo da se promeni? šta ako jedan čovek ima mogućnost da se vrati kroz vreme i izmeni tok istorije?\r\n\r\nDžejk Eping je tridesetpetogodišnji nastavnik engleskog jezika u američkoj državi Mejn. Jednog dana, prijatelj Al će mu otkriti neverovatnu tajnu: u njegovoj ostavi nalazi se vremenski portal koji vodi u 1958. godinu. Al ubeđuje Džejka da krene u neverovatnu misiju - da pokuša da spreči Kenedijevo ubistvo. Tako počinje Džejkova avantura, u kojoj kao Džordž Amberson dospeva u svet Elvisa i Džona Kenedija, upoznaje osobenjaka Lija Osvalda i lepu bibliotekarku Sejdi Danhil, koja postaje ljubav njegovog života. Džejkov novi život prevazilazi sve zakone fizike i normalne granice prostora i vremena, ali da li će to biti dovoljno da ostvari svoju misiju?\r\n\r\nOda jednom jednostavnijem vremenu i napeta misterija koja će vas ostaviti\r\nbez daha, Dalas ''63 je još jedno Kingovo remek delo epskih razmera.', 1390, 1, 0),
(100000019, 7, 'Dž.R.R. Tolkin - Deca Hurinova', 'deca hurinova.jpg', 'Epska fantastika', '&quot;Postoje priče o Srednjoj Zemlji iz vremena mnogo pre Gospodara Prstenova, a priča ispričana u ovoj knjizi smestena je u velikoj regiji koja se nalazila na zapadu iza Sivih Luka: zemlje u kojima je Dvobradi nekada hodio, ali koje su potopljene u velikoj kataklizmi kojom je okončano Prvo razdoblje sveta.\r\nU to davno vreme, Morgot, prvi Mračni Gospodar, prebivao je na Severu, u velikoj tvrđavi Angband, Gvozdenom Paklu, a tragedija Turina i njegove sestre Nienore odvijala se pod senkom straha od Angbanda i rata koji je Morgot vodio protiv vilovnjačkih zemalja i njihovih skrivenih gradova.\r\nNjihovim kratkim i strastvenim životima dominirala je iskonska mržnja koju je Morgot gajio prema njima kao deci Hurina, čoveka koji se usudio da mu prkosi i da mu se ruga u lice. Protiv njih je poslao svog najstrašnijeg slugu, Glaurunga, moćnog duha u obliku ogromnog beskrilnog plamenog zmaja. U ovoj priči o surovom osvajanju i bekstvu, šumskim skrovištima i poterama, o otporu uprkos sve manjoj nadi, Mračni Gospodar i Zmaj prikazani su izuzetno životno i jasno.\r\nZloban i podrugljiv, Glaurung manipuliše sudbinama Turina i Nienore pomoću đavolski lukavih i prepredenih laži i ispunjava Morgotovu kletvu.&quot;', 990, 1, 0),
(100000020, 4, 'Pantalone', 'katalog_b2b_15795_obrazek_zoom.jpg', 'Pantalone iz jednog dela sa V izrazom, orijentalni dizajn', 'Materijal: 100% Veštačka svila\r\nBoja: Teget\r\nVeličina: 34,36,38,40,42,44\r\n', 3500, 0, 0),
(100000021, 5, 'Kaput', 'katalog_b2b_15991_obrazek_nahled_web.jpg', 'Kaput deblji', 'Materijal: 46% Pamuk 54% Poliester\r\n\r\nBoja: Teget\r\n\r\nVeličina: S,M,L,XL,XXL,XXXL', 7000, 0, 0),
(100000022, 5, 'Farmerke', 'katalog_b2b_16442_obrazek_zoom.jpg', 'Letnje farmerke ', 'Materijal: 98% Pamuk 2% Elastin\r\n\r\nBoja: Tamno plave\r\n\r\n\r\nVeličina: 28,29,30,31,32,33,34,36,38\r\n', 4500, 0, 0),
(100000023, 5, 'Majica', 'katalog_b2b_16486_obrazek_zoom.jpg', 'Majica kratkih rukava', '\r\nMaterijal: 100% Pamuk \r\nBoja: Teget \r\nVeličina: S,M,L,XL,XXL,XXXL\r\n\r\n', 1500, 0, 0),
(100000024, 7, 'Tajna društva u Srbiji', 'tajna drustva.jpg', 'Autori: Isidora Bjelica, Nebojša Pajkić', 'Tajna društva u Srbiji &ndash; od srednjeg veka do novog milenijuma prva je knjiga koja razotkriva i prikazuje postojanje i istorijat tajnih udruženja u Srbiji i objašnjava njihove isprepletane odnose i njihov uticaj na političku stvarnost do danas. Ko su bili bogumili, templari, jovanovci i kako je to uticalo na našu istoriju, ali i na savremenu političku situaciju? Kako su malteški vitezovi uticali da glavni toposi jovanovaca budu i prioriteti Tuđmanove odbrane, koji je i sam bio malteški vitez. Uticaj Heterije, grčke tajne organizacije na Karađorđeviće i njeno suprotstavljanje Obrenovićima. Koliko danas Heterija ima uticaja na dinastičke naslednike i našu političku situaciju? Povezanost Mlade Bosne, Crne ruke i tajne komunističke partije. Ekskluzivni i poverljivi dokumenti iz ruskih, engleskih, italijanskih i nemačkih arhiva o crnoj, beloj i crvenoj ruci. Statut i konspirološka osnova Crne ruke, kao i suđenje Apisu i njegova poslednja volja... Spiskovi pripadnika organizacije. Sve o tajnoj komunističkoj partiji, Mustafi Golubiću i Crvenom kamernom orkestru... Kako je delovala tajna komunistička partija? Ko su uopšte masoni, ko su jugoslovenski i srpski masoni i kakav je njihov cilj. Delovanje teozofa, šamana, mesmerista i kabalista na našoj teritoriji. Sve o počecima i konspirologiji četničkog pokreta i njihovoj vezi sa Crnom rukom. Delovanje naših organizacija u Rusiji. Ko je kako i zašto od srednjeg veka do danas tajno radio u kojoj organizaciji i šta je bio njihov cilj? Tajna društva sedamdesetih, osamdesetih i devedesetih i instaliranje srpske političke elite od strane ezoterika i šamana dr Veska Savića; ko je i za koga je zaista radio Slobodan Milošević?', 1100, 0, 0),
(100000025, 9, 'Treći metak - Milan Veruović, Nikola Vrzić', 'treci metak.jpg', 'Politička pozadina ubistva Zorana Đinđića', 'Politička pozadina ubistva Zorana Đinđića", proizvod višegodišnjeg zajedničkog istraživanja Milana Veruovića, šefa premijerovog ličnog obezbeđenja koji je i sam teško ranjen u atentatu 12. marta 2003, i Nikole Vrzića, pomoćnika glavnog urednika nedeljnika &bdquo;Pečat". Prvi deo knjige, "12. mart 2003.", otkriva da "zvanična istina o ubistvu premijera Srbije ne počiva ni na materijalnim dokazima niti na svedočenjima očevidaca, već je iskonstruisana na neodrživim veštačenjima i na brižljivo izatkanoj mreži priznanja i saradničkih svedočenja od kojih ni jedna ni druga ne mogu da opstanu u sudaru sa neporecivim činjenicama". Drugi deo, "Politička pozadina", analizira vezu kriminala, policije i politike koja je uspostavljena posle 5. oktobra 2000, događaje koji su doveli do 12. marta 2003, umešanost inostranih struktura u formiranje zvanične istine o ubistvu Zorana Đinđića, politiku srpskog premijera i ono što je od te politike posle njegovog ubistva ostalo. A treći metak ispaljen u atentatu, koji je postao simbol svega sakrivenog i neobjašnjenog u zvaničnoj istini, pokazaće se i kao metak ispaljen u Srbiju&hellip;', 500, 0, 0),
(100000026, 7, 'Skitalac - Dejvid Gemel', 'skitalac.jpg', 'Snažna epska priča o hrabrosti, časti i iskupljenju', 'Svi Skitaočevi nagoni vrištali su na njega da ne prihvati posao od Kaima Okrutnog, čoveka koji je koštao života čitave narode. No Skitalac ih je prenebregao. Ubio je koga je trebalo da ubije. Još dok je odlazio po zlato, znao je da ga je Kaim izdao. Sada su Mračno bratstvo i kerovi haosa progonili njega, a Kaimova vojska zaratila je s Drenajem, rešena da ubije svakog muškarca, ženu i dete. Drenajske vojnike na kraju je čekao neizbežan poraz; uskoro će zavladati haos. Tada je neobičan starac saopštio Skitaocu kako postoji jedan jedini način da se ishod rata preokrene: sam Skitalac mora da donese legendarni bronzani oklop iz skrovišta u dalekim zemljama kojima hode seni. Biće progonjen. Svakako ne može uspeti. No starac mu je naložio da ipak pokuša &ndash; naložio mu je to u ime svog sina, kralja, koga je usmrtio jedan plaćeni ubica... Skitalac nipošto nije ličio na junaka &ndash; jer bio je izdajnik, krvnik, kraljeubica..', 790, 1, 0),
(100000027, 8, 'Mikelanđelo', 'mikelandjelo.jpg', 'Život i delo Mikelanđela kroz pero Romena Rolana', 'Bio je firentinski građanin - one Firence sa mračnim palatama, kulama što štrče kao koplja, sa vitkim i suvim brdima, tanano izrezbarenim na ljubičastom nebu, sa crnim vretenima svojih malih kiparisa i srebrnim pojasevima kao talasi drhtavih maslinjaka - one Firence sa zaoštrenom elegancijom u kojoj se sretalo bledunjavo ironično lice Lorenca Medičija i lukavo lice Makijavelija sa velikim ustima &bdquo;Proleća&rdquo; i Botičelijevom bledunjavom Venerom sa zlatnom kosom - one Firence što je grozničava, ponosna, nervozna, postajala plen svih fanatizama, potresana svim verskim i socijalnim histerijama, u kojoj je svako bio slobodan i svako bio tiranin, u kojoj se tako lepo dalo živeti i u kojoj je život bio pakao - ovog grada sa građanima inteligentnim, netrpeljivim, oduševljenim, mržnjom nadahnutim, jezika oštrog, a sumnjičavog duha, građana koji se uzajamno uhode, zavide jedni drugima i proždiru jedni druge - ovoga grada u kome nije bilo mesta za slobodni duh Leonarda, u kome je Botičeli završio u halucinacijama kao kakav škotski puritanac - u kome je Savonarola kozjeg profila, užagrenih očiju, izvodio svoje kaluđere u kolo oko lomače koja je spaljivala umetnička dela i gde je, tri godine kasnije, podignuta lomača da spali i samog proroka. Upravo iz ovoga grada i iz ovoga doba, Mikelanđelo je imao sva njihova predubeđenja, njihove strasti i njihove groznice...', 990, 0, 0),
(100000028, 8, 'Teško izvojevan mir', 'tesko-izvojevan-mir.jpg', 'Autobiografsko delo Nila Janga', 'Legendarni muzičar i tekstopisac Neil Young ovim iskrenim i duhovitim memoarima vodi nas na gitarsko, hipnotičko putovanje kroz svoju karijeru koja traje više od 40 godina. Teško izvojevani mir sumira uspone i padove ovog jedinstvenog umetnika. Naročitu vrednost ovoj knjizi daje to što nije jedna od onih autobiografija rok zvezda na koje smo navikli. Budući da ju je pisao Young lično, bez ghostwritera, ona je u stvari zbornik refleksija o ljubavi, muzici, drogama, porodici, a uz sve to dočarava i uzbudljive trenutke njegovog života: detinjstvo u Kanadi, učešće u rok bumu šezdesetih kao član bendova Buffalo Springfield i Crosby, Stills, Nash &amp; Young, potom karijeru s Crazy Horseom, politički i ekološki aktivizam, kao i mnogobrojne lične izazove...', 1399, 0, 0),
(100000029, 10, 'Istorija sveta', 'istorija sveta.jpg', 'Izdanje: NATIONAL GEOGRAPHIC', 'Monumentalno i sveobuhvatno, ovo bogato ilustrovano izdanje predstavlja istoriju sveta od osvita civilizacije pa sve do XXI veka na precizan, zanimljiv i izuzetno pristupačan način. Više od 4.000 slika oživljava priču o velikim carstvima na vrhuncu moći, epohalnim bitkama koje su odredile tok istorije, moćnim vladarima koji su oblikovali čitava razdoblja i velikanima čija su učenja začela najuticajnije ideologije čovečanstva. Uz njih ćete pokrenuti maštu i poželećete da saznate što više o fascinantnom čovekovom putovanju kroz istoriju. Višemilenijumski razvoj civilizacije prikazan je kroz najvažnije teme, događaje i ličnosti koje se obeležile svoje doba. Posebni posteri detaljno predstavljaju prelomne trenutke u istoriji, poput Francuske revolucije ili Drugog svetskog rata, a mnoštvo posebnih odeljaka na svakoj strani obezbeđuje lak i jednostavan pristup najvažnijim informacijama. Svi najvažniji elementi društvene, kulturne i vojne istorije sabrani su u ovaj raskošno ilustrovani vodič kroz veličanstvenu avanturu za čitaoce: uz Istoriju sveta imaće priliku da sagledaju sjaj egipatskih piramida, prošetaju sa Cezarom ulicama drevnog Rima, pođu na pohod sa Napoleonom i zapute se u svemir sa Sputnikom. Izuzetna po svom obimu i stručnjacima koji je potpisuju, ova enciklopedija predstavlja dragocenu hroniku istorije čovečanstva elegantno smeštenu u jedno bogato, precizno i nadasve zanimljivo izdanje.', 6500, 0, 0),
(100000030, 10, 'Drugi svetski rat - Entoni Bivor', 'svetski rat.jpg', 'Definitivna istorija. Ovo je Drugi svetski rat kako bi ga Tolstoj opisao.', 'Drugi svetski rat počeo je avgusta 1939. na granici Mandžurije, a tačno šest godina kasnije tamo se i završio sovjetskom invazijom na severnu Kinu. Rat u Evropi deluje potpuno odvojeno od rata na Pacifiku i u Kini, ali događaji na suprotnim stranama sveta imali su međusobno duboke posledice. Služeći se najsavremenijim naučnim otkrićima i izvorima i pišući jasno i saosećajno, Entoni Bivor sklapa ogroman mozaik koji obuhvata sve od severnoafričke pustinje preko snegom okovanih stepa do džungli Burme, od esesovskih specijalnih odreda preko zatočenika Gulaga regrutovanih u kaznene bataljone do neizrecivih zverstava na kineskom ratištu.', 2400, 0, 0),
(100000031, 6, 'Chicco haljina', 'haljina devojcice.jpg', 'Pamučna haljinica za devojčice', 'Chicco haljina \r\nSastav: 100% pamuk, \r\nPostava: 100% pamuk,\r\nOdržavanje: Ručno pranje', 2990, 0, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kupovine`
--
ALTER TABLE `kupovine`
  ADD CONSTRAINT `kupovina_korisnik` FOREIGN KEY (`Korisnik`) REFERENCES `korisnici` (`IDKorisnika`);

--
-- Constraints for table `proizvodi`
--
ALTER TABLE `proizvodi`
  ADD CONSTRAINT `proizvod_kategorija` FOREIGN KEY (`Kategorija`) REFERENCES `kategorije` (`IDKategorije`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
