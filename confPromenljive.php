<?php
/* Instanciranje globalnih promenljivih */

//Lista strana u slucaju da neko menja sadrzaj URL adrese
$LISTA_STRANA = [
    'prijava',
    'registracija',
    'proizvodi',
    'kontakt',
    'novi_proizvodi',
    'porudzbine',
    'korpa',
    'profil',
];

//Promenljive vezane za konekciju na bazu
$DB_DSN = "mysql:host=localhost;dbname=internetprodavnica";
$DB_USERNAME = "crinetic_admin";
$DB_PASSWORD = "SifrA4CriN";

//Valuta za prikaz
$VALUTA = "RSD";
