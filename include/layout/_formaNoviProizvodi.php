<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body ">
                <form id="novi_proizvod" action="" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="glavne_kategorije">Izaberite kategoriju:</label>
                        <select class="form-control" name="glavnaKategorija">
                                <option selected></option>
                            <?php foreach ($_SESSION['kategorije'] as $kategorija => $podkategorije): ?>
                                <option value="<?php echo $kategorija; ?>"><?php echo ucfirst($kategorija); ?></option>
                            <?php endforeach; ?>
                        </select>
                        <br>
                        <label for="Kategorija">Izaberite podkategoriju:</label>
                        <select class="form-control" name="Kategorija">
                            <!-- Ajax ovo popunjava -->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="Naziv">Naziv proizvoda:</label>
                        <input class="form-control" type="text" name="Naziv">
                    </div>
                    <div class="form-group">
                        <label for="Kratakopis">Kratak opis:</label>
                        <input class="form-control" type="text" name="Kratakopis">
                    </div>
                    <div class="form-group">
                        <label for="Dugiopis">Dugi opis:</label>
                        <textarea class="form-control" type="text" name="Dugiopis" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="Cena">Cena:</label>
                        <input class="form-control" type="text" name="Cena">
                    </div>
                    <div class="form-group">
                        <label for="Slika">Slika:</label>
                        <input class="form-control" type="file" name="Slika">
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default" type="submit" name="submit" value="1">Unesi proizvod</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>