<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body ">
                <form id="registracija" action="" method="POST">
                    <div class="form-group">
                        <label for="username">Korisničko ime:</label>
                        <input class="form-control" type="text" name="korisnickoIme">
                    </div>
                    <div class="form-group">
                        <label for="sifra">Lozinka:</label>
                        <input class="form-control" type="password" name="sifra">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail:</label>
                        <input class="form-control" type="text" name="email">
                    </div>
                    <div class="form-group">
                        <label for="adresa">Adresa:</label>
                        <input class="form-control" type="text" name="adresa">
                    </div>
                    <div class="form-group">
                        <label for="mobilni">Mobilni telefon:</label>
                        <input class="form-control" type="text" name="mobilni" pattern="\d{3}[\-]\d{6,7}" title="Neispravan format telefona. Ispravan format: XXX-XXXXXXX.">
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default" type="submit" name="submit" value="1">Registruj se</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>