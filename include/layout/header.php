<?php
//Provera dali postoje parametri za kategoriju i podkategoriju u URL adresi
$kategorija = isset($_GET['kategorija']) ? $_GET['kategorija'] : "";
$podkategorija = isset($_GET['podkategorija']) ? $_GET['podkategorija'] : "";

//Deklaracija promenljive koja ce sadrzati naslov
$naslov_header = "";

//Provere dali je kliknuta kategorija ili podkategorija i smestanje u promenljivu $naslov_header za prikaz
if ($kategorija != "") {
    $naslov_header .= $kategorija;

    if ($podkategorija != "") {
        $naslov_header .= ' / ' . $podkategorija;
    }

} else {
    $naslov_header = "Dobrodošli !";
}

//Provera dali Session promenljiva za kategorije postoji, ako ne, popuni je vrednostima iz baze.
//Ovim se rasterecuje baza, kako nebi isli po kategorije na svaki request u aplikaciji.
//Takodje aplikacija ce otici do baze samo prilikom pokretanja (jedanput)
if (!isset($_SESSION['kategorije']) || $_SESSION['kategorije'] != "") {
    require_once('./include/skripte/getKategorije.php');
    getKategorije();
}
//Lokalna promenljiva za ispis kategorija
$kategorije = $_SESSION['kategorije'];

?>

<!-- Header -->
<div class="row">
    <div class="col-md-12">

        <!-- Info deo iznad navigacije-->
        <div class="page-header text-center">
            <h1>Internet prodavnica</h1>
            <h3><?php echo $naslov_header; ?></h3>
        </div>

        <!-- Navigacija -->
        <nav class="navbar navbar-default" role="navigation">

            <!-- Dugme koje se pojavljuje u responsivu-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
            </div>

            <!-- Sadrzaj navigacije-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="./">Naslovna</a>
                    </li>

                    <!-- Petlja za ispis i formiranje linkova svih kategorija -->
                    <?php foreach ($kategorije as $kategorija => $podkategorije): ?>
                        <?php //var_dump($kategorija); die(); ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $kategorija; ?><strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <?php
                                /* Petlja za ispis i formiranje linkova za podkategorije koji pripadaju trenutnoj kategoriji u petlji*/
                                foreach ($podkategorije as $podkategorija):
                                    ?>
                                    <li>
                                        <a href="?strana=proizvodi&kategorija=<?php echo $kategorija; ?>&podkategorija=<?php echo $podkategorija; ?>"><?php echo $podkategorija; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endforeach; ?>
                    <!-- /Petlja za ispis i formiranje linkova svih kategorija -->

                    <!-- Dinamicki meni -->
                    <?php if (empty($KORISNIK)): ?>
                        <!-- Dugme prijavi se za neprijavljene korisnike -->
                        <li>
                            <a href="?strana=prijava">Prijavi se</a>
                        </li>
                    <?php elseif ($KORISNIK['administrator'] == 1): ?>
                        <!-- Meni samo za administratorski nalog -->
                        <li class="dropdown administrator">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administracija<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="?strana=novi_proizvodi">Novi proizvodi</a>
                                </li>
                                <li>
                                    <a href="?strana=porudzbine">Porudžbine</a>
                                </li>
                                <li>
                                    <a href="?odjava=true">Odjavi se</a>
                                </li>
                            </ul>
                        </li>
                    <?php else: ?>
                        <!-- Meni za prijavljenog korisnika -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profil<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="?strana=profil">Uredi profil</a>
                                </li>
                                <li>
                                    <a href="?odjava=true">Odjavi se</a>
                                </li>
                            </ul>
                        </li>
                        <li class="korpa">
                            <a href="?strana=korpa">Korpa (<strong id="korpaBroj"><?php echo $KORISNIK['korpaBrojProizvoda']; ?></strong>)</a>
                        </li>
                    <?php endif; ?>
                    <!-- /Dinamicki meni -->

                    <li>
                        <a href="?strana=kontakt">Kontakt</a>
                    </li>
                </ul>
            </div>
            <!-- /Sadrzaj navigacije-->
        </nav>
        <!-- /Navigacija -->
    </div>
</div>