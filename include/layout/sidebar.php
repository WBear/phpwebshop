<?php

$query_sidebar = $db->prepare("SELECT * FROM `proizvodi` `p` ORDER BY `p`.`BrojSvidjanja` DESC LIMIT 3");
$query_sidebar->execute();
$popularniProizvodi_lista = $query_sidebar->fetchALL(PDO::FETCH_ASSOC);
$query_sidebar->closecursor();
?>

<div><label>Popularni proizvodi:</label></div>

<?php foreach ($popularniProizvodi_lista as $popularniProizvod): ?>

    <div class="panel panel-default proizvod-panel">
        <div class="panel-head text-center">
            <h4><?php echo $popularniProizvod['Naziv']; ?></h4>
        </div>
        <div class="panel-body">
            <a class="detaljnije" data-toggle="modal" data-target="#proizvodModal" data-id="<?php echo $popularniProizvod['IDProizvoda']; ?>">
                <p><img class="cursor" src="./slike_proizvodi/<?php echo $popularniProizvod['Slika']; ?>" width="60"/></p>
            </a>
            <p class="text-left proizvod-opis"><i><?php echo $popularniProizvod['Kratakopis']; ?></i></p>
            Broj sviđanja: <label><?php echo $popularniProizvod['BrojSvidjanja']; ?></label>
            <br><br>
            <label class="pull-right">Cena: <?php echo $popularniProizvod['Cena'] . $VALUTA; ?></label>
        </div>
    </div>

<?php endforeach; ?>