<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body ">
                <form id="profil" action="" method="POST">
                    <div class="form-group">
                        <label for="korisnickoIme">Korisničko ime:</label>
                        <input class="form-control" type="text" name="korisnickoIme" disabled value="<?php echo $_SESSION['korisnik']['korisnickoIme']; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="ime">Ime:</label>
                        <input class="form-control" type="text" name="ime" value="<?php echo $_SESSION['korisnik']['Ime']; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="prezime">Prezime:</label>
                        <input class="form-control" type="text" name="prezime" value="<?php echo $_SESSION['korisnik']['Prezime']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input class="form-control" type="text" name="email" value="<?php echo $_SESSION['korisnik']['Email']; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="mobilni">Mobilni telefon:</label>
                        <input class="form-control" type="text" name="mobilni" pattern="\d{3}[\-]\d{6,7}" title="Neispravan format telefona. Ispravan format: XXX-XXXXXXX." value="<?php echo
                        $_SESSION['korisnik']['Mobilni']; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="adresa">Adresa:</label>
                        <input class="form-control" type="text" name="adresa" value="<?php echo $_SESSION['korisnik']['Adresa']; ?>" />
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default" type="submit" name="submit" value="1">Sačuvaj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>