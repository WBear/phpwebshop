<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
<div style='overflow:hidden;height:360px;width:280px;'>
    <div id='gmap_canvas' style='height:360px;width:280px;'></div>
    <style>
        #gmap_canvas img {
            max-width: 280px !important;
            background: 500px !important
        }
    </style>
</div>
<script type='text/javascript'>
    function init_map() {
        var myOptions = {zoom: 10, center: new google.maps.LatLng(44.80925570186307, 20.462823256591815), mapTypeId: google.maps.MapTypeId.ROADMAP};
        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
        marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(44.80925570186307, 20.462823256591815)});
        infowindow = new google.maps.InfoWindow({content: '<strong>Internet prodavnica d.o.o.</strong><br>Kneza Milosa 1a<br>'});
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
        infowindow.open(map, marker);
    }
    google.maps.event.addDomListener(window, 'load', init_map);
</script>