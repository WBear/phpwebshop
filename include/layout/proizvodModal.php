<!-- Proizvod Modal -->
<div class="modal fade" id="proizvodModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="proizvodModalNaslov"></h4>
            </div>
            <div class="modal-body">
                <div class="text-left col-md-6">
                    <img id="proizvodModalSlika" width="270"/>
                </div>
                <div class="text-left col-md-6">
                    <p id="proizvodModalOpis"></p>
                    <label class="pull-right">Cena: <span id="proizvodModalCena"></span><?php echo $VALUTA; ?> </label>
                </div>
            </div>
            <div class="modal-footer">
                <?php if (sizeof($KORISNIK) > 0 && $KORISNIK['administrator'] == 0): ?>
                    <label for="kolicina">Količina: </label>
                    <input id="proizvodModalKolicina" type="number" min="1" value="1"/>
                    <input id="proizvodModalId" type="hidden" value=""/>
                    <button id="uKorpu" type="button" class="btn btn-success">U korpu</button>
                <?php else: ?>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="./?strana=prijava" class="btn btn-sm alert-info text-center">
                                <b>Prijavite se, da bi ste dodali ovaj proizvod <br>u korpu.</b>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>