<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body ">
                <form id="logovanje" action="" method="POST">
                    <div class="form-group">
                        <label for="username">Korisničko ime:</label>
                        <input class="form-control" type="text" name="korisnickoIme">
                    </div>
                    <div class="form-group">
                        <label for="sifra">Lozinka:</label>
                        <input class="form-control" type="password" name="sifra">
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default" type="submit" name="submit" value="1">Prijavi se</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <p><label>Ukoliko nemate nalog, registrujte se.</label></p>
                <a class="btn btn-default" href="?strana=registracija">Registruj se</a>
            </div>
        </div>
    </div>
</div>