<?php

include('../../confPromenljive.php');
include('./dbKonekcija.php');

$ime_kategorije = $_POST['imekategorije'];

//Pronadji ID za ime kategorije
$query_kategorija = $db->prepare("SELECT `IDKategorije` FROM `kategorije` k WHERE `k`.`Ime` = ?");
$query_kategorija->execute(array($ime_kategorije));
$kategorija_id = $query_kategorija->fetch(PDO::FETCH_ASSOC);
$query_kategorija->closecursor();

//Pronadji podkategorije koje pripadaju izabranoj kategoriji
$query_podkategorije = $db->prepare("SELECT * FROM `kategorije` k WHERE `k`.`Podkategorija` = ?");
$query_podkategorije->execute(array($kategorija_id['IDKategorije']));
$podkategorije_lista = $query_podkategorije->fetchAll(PDO::FETCH_ASSOC);
$query_podkategorije->closecursor();

echo json_encode($podkategorije_lista);