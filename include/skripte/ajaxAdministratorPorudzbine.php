<?php
session_start();

include('../../confPromenljive.php');
include('./dbKonekcija.php');
include('./funkcije.php');

//Setovanje id porudzbine iz ajax poziva i akcije (isporuci -ako je true, obrisi ako je false)
$porudzbinaId = $_POST['idPorudzbine'];
$isporuci = $_POST['isporuci'];

//Provera dali je zashtev zaista poslao administrator, ako nije vrati ga na pocetnu stranu
if (isset($KORISNIK['administrator']) && $KORISNIK['administrator'] == 1) {
    if ($isporuci == "isporuci") {
        //Update porudzbine setovanjem kolone `Isporuceno` (setuje se datum isporuke)
        $query_porudzbina = $db->prepare("UPDATE `kupovine` k SET `k`.`Isporuceno`= NOW() WHERE `k`.`IDKupovine` = ?");
        $query_porudzbina->execute(array($porudzbinaId));
        $query_porudzbina->closecursor();
    } elseif ($isporuci == "obrisi") {
        //Obrisi porudzbinu
        $query_porudzbina = $db->prepare("DELETE FROM `kupovine` WHERE `IDKupovine` = ?");
        $query_porudzbina->execute(array($porudzbinaId));
        $query_porudzbina->closecursor();
    }
    echo "true";
} else {
    echo "false";
}
