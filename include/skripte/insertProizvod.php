<?php

function insertProizvod()
{
    if (isset($_SESSION['korisnik']['administrator']) && $_SESSION['korisnik']['administrator'] == 1) {
        global $db;

        //Pokupi sva polja sa forme za registraciju
        $kategorija = $_POST['Kategorija'];
        $naziv = $_POST['Naziv'];
        $kratakopis = $_POST['Kratakopis'];
        $dugiopis = $_POST['Dugiopis'];
        $cena = $_POST['Cena'];
        $slika = $_FILES["Slika"]["name"];

        $query_unesi_proizvod = $db->prepare("INSERT INTO `proizvodi`(`Kategorija`, `Naziv`, `Slika`, `Kratakopis`, `Dugiopis`, `Cena`) VALUES (?, ?, ?, ?, ?, ?)");
        $query_unesi_proizvod->execute(array($kategorija, $naziv, $slika, $kratakopis, $dugiopis, $cena));
        $query_unesi_proizvod->closecursor();

        move_uploaded_file($_FILES["Slika"]["tmp_name"], "./slike_proizvodi/" . $_FILES["Slika"]["name"]);

        return true;
    } else {
        return false;
    }
}