<?php
function getPodkategorijaProizvodi($podkategorija){
    global $db;
    $query_proizvodi = $db->prepare("SELECT * FROM `proizvodi` p
                                     LEFT JOIN `kategorije` k
                                        ON `p`.`Kategorija` = `k`.`IDKategorije`
                                      WHERE `k`.`Ime` = ?");
    $query_proizvodi->execute(array($podkategorija));
    $proizvodi_lista = $query_proizvodi->fetchAll(PDO::FETCH_ASSOC);
    $query_proizvodi->closecursor();

    return $proizvodi_lista;
}