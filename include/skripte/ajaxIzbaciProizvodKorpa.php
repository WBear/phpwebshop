<?php
session_start();

include('../../confPromenljive.php');
include('./dbKonekcija.php');
include('./funkcije.php');

//Setovanje id proizvoda iz ajax poziva
$proizvodId = $_POST['id'];

//Provera dali se ovaj proizvod vec nalazi u korpi
if (isset($_SESSION['korisnik']['Kolica'][$proizvodId])) {
    unset($_SESSION['korisnik']['Kolica'][$proizvodId]);

    //Spremi za upis u bazu u json formatu
    $kolica = json_encode($_SESSION['korisnik']['Kolica']);

    //Update korisnik -> kolica u bazi sa vrednoscu
    $query_korpa = $db->prepare("UPDATE `korisnici` k SET `k`.`Kolica`= ? WHERE `k`.`IDKorisnika` = ?");
    $query_korpa->execute(array($kolica, $KORISNIK['IDKorisnika']));
    $query_korpa->closecursor();

    //Update broj proizvoda u session promenljivi -za prikaz
    $_SESSION['korisnik']['korpaBrojProizvoda'] = $_SESSION['korisnik']['korpaBrojProizvoda'] - 1;
}
echo "true";