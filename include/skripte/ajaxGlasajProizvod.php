<?php
session_start();

include('../../confPromenljive.php');
include('./dbKonekcija.php');
include('./funkcije.php');

//Setovanje id proizvoda iz ajax poziva
$proizvodId = $_POST['proizvodid'];
$tip = $_POST['tip'];

//Provera dali je korisnik ulogovan
if (!empty($KORISNIK)) {
    if ($tip == "gore") {
        //Update za Like
        $query_like = $db->prepare("UPDATE `proizvodi` SET `BrojSvidjanja`= `BrojSvidjanja` + 1 WHERE `IDProizvoda` = ?");
        $query_like->execute(array($proizvodId));
        $query_like->closecursor();
    } elseif ($tip == "dole") {
        //Update za Like
        $query_like = $db->prepare("UPDATE `proizvodi` SET `BrojNesvidjanja`= `BrojNesvidjanja` + 1 WHERE `IDProizvoda` = ?");
        $query_like->execute(array($proizvodId));
        $query_like->closecursor();
    }

}