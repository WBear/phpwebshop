<?php

$dsn = $DB_DSN;
$username = $DB_USERNAME;
$password = $DB_PASSWORD;
try {
	$db = new PDO($dsn, $username, $password);
	$db->exec("set names utf8");

} catch (PDOExecption $e) {
	$error_msg = $e->getMessage();
	echo "Greska je: ".$error_msg;
}