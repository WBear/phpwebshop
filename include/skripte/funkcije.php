<?php

//Smesti ulogovanog korisnika u promenljivu radi lakse manipulacije u aplikaciji
if (isset($_SESSION['korisnik']) && $_SESSION['korisnik'] != "") {
    $KORISNIK = $_SESSION['korisnik'];
} else {
    $KORISNIK = [];
}

//Provera dali je korisnik pokusao da se izloguje, ako jeste izloguj ga tako sto ces isprazniti sesiju
if (isset($_GET['odjava']) && $_GET['odjava']) {
    unset($_SESSION['korisnik']);
    $KORISNIK = [];
}


