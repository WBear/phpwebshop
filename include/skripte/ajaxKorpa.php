<?php
session_start();

include('../../confPromenljive.php');
include('./dbKonekcija.php');
include('./funkcije.php');

//Setovanje id proizvoda iz ajax poziva
$proizvodId = $_POST['proizvodid'];

//Provera dali se ovaj proizvod vec nalazi u korpi
if (isset($_SESSION['korisnik']['Kolica'][$proizvodId])) {
    //Nalazi se: uzmi kolicinu koja se vec nalazi u korpi, uvecaj je za kolicinu u ajax zahtevu i setuj promenljivu $brojProizvoda na tu vrednost
    $kolicina = $_SESSION['korisnik']['Kolica'][$proizvodId]['kolicina'] + $_POST['kolicina'];
    $brojProizvoda = $_SESSION['korisnik']['korpaBrojProizvoda'];
} else {
    //Ne nalazi se: kolicina = kolicina u ajax zahtevu i setuj promenljivu $brojProizvoda na tu vrednost
    $kolicina = $_POST['kolicina'];
    $brojProizvoda = $_SESSION['korisnik']['korpaBrojProizvoda'] + 1;
}

//Pokupi ostale informacije o proizvodu
$naziv = $_POST['naziv'];
$cena = $_POST['cena'];

//Update session promenljive za proizvod u ajax zahtevu
//Ako vec postoji uradice samo update kolicine, ako ne, dodace u listu 'Kolica'
$_SESSION['korisnik']['Kolica'][$proizvodId] = ['kolicina' => $kolicina, 'naziv' => $naziv, 'cena' => $cena];
//Spremi za upis u bazu u json formatu
$kolica = json_encode($_SESSION['korisnik']['Kolica']);

//Update korisnik -> kolica u bazi sa vrednoscu
$query_korpa = $db->prepare("UPDATE `korisnici` k SET `k`.`Kolica`= ? WHERE `k`.`IDKorisnika` = ?");
$query_korpa->execute(array($kolica, $KORISNIK['IDKorisnika']));
$query_korpa->closecursor();

//Update broj proizvoda u session promenljivi -za prikaz
$_SESSION['korisnik']['korpaBrojProizvoda'] = $brojProizvoda;

echo $brojProizvoda;