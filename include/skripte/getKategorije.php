<?php

function getKategorije() {
    global $db;

    //Upit koji vraca sve kategorije iz baze i smesta u listu
    $query_kategorije = $db->prepare("SELECT * FROM `kategorije` k WHERE `k`.`Podkategorija` IS NULL");
    $query_kategorije->execute();
    $kategorije_lista = $query_kategorije->fetchALL(PDO::FETCH_ASSOC);
    $query_kategorije->closecursor();

    foreach ($kategorije_lista as $kategorija) {
        /* Upit koji vraca podkategorije za trenutnu kategoriju u petlji*/
        $query_podkategorije = $db->prepare("SELECT * FROM `kategorije` k WHERE `k`.`Podkategorija` = ?");
        $query_podkategorije->execute(array($kategorija['IDKategorije']));
        $podkategorije_lista = $query_podkategorije->fetchALL(PDO::FETCH_ASSOC);
        $query_podkategorije->closecursor();

        foreach ($podkategorije_lista as $podkategorija) {
            //Napuni Session promenljivu
            $kategorije[$kategorija['Ime']][] = $podkategorija['Ime'];
        }
    }
    $_SESSION['kategorije'] = $kategorije;
}