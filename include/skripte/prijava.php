<?php

function prijava()
{
    global $db;

    //Pokupi sva polja sa forme za prijavu i sanitizuj unose sa funkcijama htmlentities i strip_tags.
    $korisnickoIme = $_POST['korisnickoIme'];
    $sifra = $_POST['sifra'];

    //Upit koji proverava dali postoji vec korisnicko ime, vraca broj korisnika.
    $query_korisnik_postoji = $db->prepare("SELECT * FROM `korisnici` k WHERE `k`.`korisnickoIme` = ? AND `Sifra` = ?");
    $query_korisnik_postoji->execute(array($korisnickoIme, $sifra));
    $korisnik_postoji = $query_korisnik_postoji->fetch(PDO::FETCH_ASSOC);
    $query_korisnik_postoji->closecursor();

    //Provera dali je nasao u bazi nekog korisnika sa ovim korisnickim imenom i sifrom, ako nije funkcija vraca false.
    if (empty($korisnik_postoji))
        return false;


    //Prosao je dovde, prijava je uspesna. Stavi podatke o korisniku u sesiju.
    $_SESSION['korisnik'] = $korisnik_postoji;
    $_SESSION['korisnik']['Kolica'] = json_decode($korisnik_postoji['Kolica'], true);
    $_SESSION['korisnik']['korpaBrojProizvoda'] = count((array)$_SESSION['korisnik']['Kolica']);
    //var_dump($_SESSION['korisnik']['korpaBrojProizvoda']);
    //die();

    return true;
}