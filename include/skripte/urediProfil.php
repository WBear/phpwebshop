<?php

function urediProfil() {

    global $db;

    //Pokupi sva polja sa forme za update profila i sanitizuj unose sa funkcijama htmlentities i strip_tags.
    $ime = $_POST['ime'];
    $prezime = $_POST['prezime'];
    $email = $_POST['email'];
    $adresa = $_POST['adresa'];
    $mobilni = $_POST['mobilni'];
    $korisnikId = $_SESSION['korisnik']['IDKorisnika'];

    $query_unesi_proizvod = $db -> prepare("UPDATE `korisnici` `k`
                                            SET `k`.`Ime` = ?, `k`.`Prezime` = ?, `k`.`Email` = ?, `k`.`Adresa` = ?, `k`.`Mobilni` = ?
                                            WHERE `k`.`IDKorisnika` = ?");
    $query_unesi_proizvod -> execute(array($ime, $prezime, $email, $adresa, $mobilni, $korisnikId));
    $query_unesi_proizvod->closecursor();

    //Update session promenljive 'korisnik' sa novim vrednostima
    $_SESSION['korisnik']['Ime'] = $ime ;
    $_SESSION['korisnik']['Prezime'] = $prezime ;
    $_SESSION['korisnik']['Email'] = $email ;
    $_SESSION['korisnik']['Adresa'] = $adresa ;
    $_SESSION['korisnik']['Mobilni'] = $mobilni ;

    return true;
}