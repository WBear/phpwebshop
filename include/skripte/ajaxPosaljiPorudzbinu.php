<?php

session_start();

include('../../confPromenljive.php');
include('./dbKonekcija.php');

//Spremi za upis u bazu u tabelu `kupovine` u json formatu
$_SESSION['korisnik']['Kolica']['korisnik'] = [
                                                'imePrezime' => $_SESSION['korisnik']['Ime'] . ' ' . $_SESSION['korisnik']['Prezime'],
                                                'adresa' => $_SESSION['korisnik']['Adresa'],
                                                'mobilni' => $_SESSION['korisnik']['Mobilni']
                                                ];

$kolica = json_encode($_SESSION['korisnik']['Kolica']);

//Insert porudzbinu u tabelu `kupovine`
$query_kupovina = $db->prepare("INSERT INTO `kupovine`(`Korisnik`, `Kupljeniproizvodi`)  VALUES (?, ?)");
$query_kupovina->execute(array($_SESSION['korisnik']['IDKorisnika'], $kolica));
$query_kupovina->closecursor();

//Update korisnik -> kolica. Isprazni korpu
$query_korpa = $db->prepare("UPDATE `korisnici` k SET `k`.`Kolica`= '' WHERE `k`.`IDKorisnika` = ?");
$query_korpa->execute(array($_SESSION['korisnik']['IDKorisnika']));
$query_korpa->closecursor();

//Update broj proizvoda u session promenljivi -za prikaz
$_SESSION['korisnik']['Kolica'] = "";
$_SESSION['korisnik']['korpaBrojProizvoda'] = 0;

echo "true";