<?php

function registracija()
{
    global $db;

    //Pokupi sva polja sa forme za registraciju i sanitizuj unose sa funkcijama htmlentities i strip_tags.
    $korisnickoIme = $_POST['korisnickoIme'];
    $email = $_POST['email'];
    $sifra = $_POST['sifra'];
    $adresa = $_POST['adresa'];
    $mobilni = $_POST['mobilni'];

    //Upit koji proverava dali postoji vec korisnicko ime, vraca broj korisnika.
    $query_korisnik_postoji = $db->prepare("SELECT * FROM `korisnici` k WHERE `k`.`korisnickoIme` = ?");
    $query_korisnik_postoji->execute(array($korisnickoIme));
    $korisnik_postoji = $query_korisnik_postoji->rowCount();
    $query_korisnik_postoji->closecursor();

    //Provera dali je nasao u bazi nekog korisnika sa ovim korisnickim imenom ako jeste funkcija vraca false.
    if ($korisnik_postoji != 0)
        return false;

    //Prosao je dovde, znaci ne postoji korisnik sa ovim korisnickim imenom. Ubaci ga u bazu.
    //Usput uzmi id tog novog korisnika
    $query_novi_korisnik = $db->prepare("INSERT INTO `korisnici`(`korisnickoIme`, `Email`, `Sifra`, `Adresa`, `Mobilni`) VALUES (?, ?, ?, ?, ?)");
    $query_novi_korisnik->execute(array($korisnickoIme, $email, $sifra, $adresa, $mobilni));
    $query_novi_korisnik->closecursor();

    //Izvuci novog korisnika iz baze zbog id, administrator,.... promenljivih i smesti ga u sesiju
    $query_korisnik = $db->prepare("SELECT * FROM `korisnici` k WHERE `k`.`korisnickoIme` = ?");
    $query_korisnik->execute(array($korisnickoIme));
    $korisnik = $query_korisnik->fetch(PDO::FETCH_ASSOC);
    $query_korisnik->closecursor();

    $_SESSION['korisnik'] = $korisnik;

    return true;
}