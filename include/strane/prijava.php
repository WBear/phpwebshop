<?php

//Provera dali je forma prosledjena i dali je uspesna prijava
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    include('./include/skripte/prijava.php');

    //Provera dali je uspesna prijava
    if (prijava()): ?>
        <!-- Poruka o uspesnoj prijavi -->
        <div class="panel panel-default">
            <div class="panel-body alert-success text-center">Uspešno ste se prijavili.</div>
        </div>

        <!-- Automatski redirektuj korisnika na pocetnu stranu 2 sekunde -->
        <script type="text/javascript">
            setTimeout(function() {
                window.location.href = "./";
            }, 1500);
        </script>

    <?php else: ?>
        <!-- Poruka o neuspeloj prijavi -->
        <div class="panel panel-default">
            <div class="panel-body alert-danger text-center">Pogrešna kombinacija korisničkog imena i šifre.</div>
        </div>
        <?php
        /* Prikazi ponovo formu za prijavu */
        include('./include/layout/_formaLogovanje.php');
    endif;

} else {
    /* Forma nije prosledjena, prikazi formu za prijavu */
    include('./include/layout/_formaLogovanje.php');
}

