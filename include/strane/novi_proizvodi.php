<?php
//Dozvoli samo administratorima ovu stranu
if (isset($KORISNIK['administrator']) && $KORISNIK['administrator'] == 1):

//Provera dali je forma prosledjena
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        include('./include/skripte/insertProizvod.php');

        //Provera dali je proizvod uspesno dodat
        if (insertProizvod()): ?>
            <!-- Poruka o uspesno dodatom proizvodu -->
            <div class="panel panel-default">
                <div class="panel-body alert-success text-center">Proizvod uspešno dodat u bazu.</div>
            </div>

        <?php else: ?>
            <!-- Poruka da proizvod pod tim imenom vec postoji u bazi -->
            <div class="panel panel-default">
                <div class="panel-body alert-danger text-center">Proizvod pod tim imenom postoji u bazi.</div>
            </div>
            <?php
        endif;
    }

    /* Prikazi formu za nove proizvode */
    include('./include/layout/_formaNoviProizvodi.php');

else: ?>
    <!-- Poruka da proizvod pod tim imenom vec postoji u bazi -->
    <div class="panel panel-default">
        <div class="panel-body alert-danger text-center">OVA STRANICA JE NAMENJENA ADMINISTRATORIMA.</div>
    </div>
<?php endif; ?>







