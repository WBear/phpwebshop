<div class="row">
    <div class="col-md-12">
        <p><h3>Preporuka: Najbolji telefoni na našem tržištu</h3></p>
        <p>
            Do kraja godine neće biti velikih premijera na tržištu smartfonova, pa će na novitete morati da se čeka do 2017.
            Među ovogodišnjim modelima uglavnom dominira jak hardver i veliki ekran, a mi smo izdvojili dva najzanimljivija koji se mogu naći na našem tržištu.
        </p>
    </div>

    <div class="col-md-12">
        <div class="col-md-6 text-center">
            <img src="./assets/slike/Iphone.jpg" style="width:170px;height:150px;">
            <p>Iphone5</p>
        </div>

        <div class="col-md-6 text-center">
            <img src="./assets/slike/HTC.jpg" style="width:170px;height:150px;">
            <p>HTC</p>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <p><h3>Mali princ</h3></p>
        <p><i>Antoan de Sent Egziperi</i></p>
        <p>...Kažete li im: „Dokaz da je mali princ postojao jeste to što je bio divan, što se smejao, što je želeo ovcu.
            Kad čovek želi ovcu, to je dokaz da postoji“, oni će slegnuti ramenima i smatraće vas detetom! Međutim, ukoliko im saopštite:
            „Planeta s koje je došao mali princ jeste asteroid B612“, biće zadovoljni i neće vam više dosađivati pitanjima. Takvi su oni.
            Ne valja se zato na njih ljutiti. Deca moraju mnogo toga da praštaju odraslima.</p>
        <div class="text-center">
            <img src="./assets/slike/princ.jpg" style="width:100px;height:150px;">
        </div>
    </div>
</div>