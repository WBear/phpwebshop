<?php

//Provere: dali je forma prosledjena
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    include('./include/skripte/urediProfil.php');

    //Provera dali je profil uspesno sacuvan
    if (urediProfil()): ?>
        <!-- Poruka o uspesno sacuvanom profilu -->
        <div class="panel panel-default">
            <div class="panel-body alert-success text-center">Profil uspešno sačuvan.</div>
        </div>

    <?php else: ?>
        <!-- Greska u slucaju da profil nije sacuvan -->
        <div class="panel panel-default">
            <div class="panel-body alert-danger text-center">Došlo je do greške prilikom čuvanja profila</div>
        </div>
        <?php
    endif;
}

/* Prikazi formu za uredjivanje profila */
include('./include/layout/_formaProfil.php');
