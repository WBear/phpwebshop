<?php

if ($_SESSION['korisnik']['korpaBrojProizvoda'] > 0):
    if ($_SESSION['korisnik']['Ime'] != "" && $_SESSION['korisnik']['Prezime'] != ""):
        $ukupna_cena = 0;
        ?>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="korpa-proizvodi">
                            <thead>
                            <tr>
                                <td class="text-left"><label>ID</label></td>
                                <td class="text-left"><label>Naziv</label></td>
                                <td class="text-right"><label>Jedinična cena</label></td>
                                <td class="text-right"><label>Količina</label></td>
                                <td class="text-right"><label>Ukupno</label></td>
                                <td class="text-center"></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($KORISNIK['Kolica'] as $proizvodId => $proizvod): ?>
                                <tr>
                                    <td class="text-left"><?php echo $proizvodId; ?></td>
                                    <td class="text-left"><?php echo $proizvod['naziv']; ?></td>
                                    <td class="text-right"><?php echo $proizvod['cena'] . ' ' . $VALUTA; ?></td>
                                    <td class="text-right"><?php echo $proizvod['kolicina']; ?></td>
                                    <td class="text-right"><?php echo $proizvod['cena'] * $proizvod['kolicina'] . ' ' . $VALUTA; ?></td>
                                    <?php $ukupna_cena = $ukupna_cena + $proizvod['cena'] * $proizvod['kolicina']; ?>
                                    <td class="text-center" onclick="izbaciProizvod(<?php echo $proizvodId; ?>);"><span class="glyphicon glyphicon-trash"></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td class="text-right" colspan="4"><label>SVE UKUPNO</label></td>
                                <td class="suma-korpa text-right"><label><?php echo $ukupna_cena . ' ' . $VALUTA; ?></label></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <p class="pull-left">
                            <button id="isprazni-korpu" class="btn btn-danger" onclick="isprazniKorpu();">Isprazni korpu</button>
                        </p>
                        <p class="pull-right">
                            <button id="posalji-porudzbinu" class="btn btn-success" onclick="posaljiPorudzbinu();">Pošalji porudžbinu</button>
                        </p>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div><label>Podaci za porudžbinu:</label></div>
                        <div>Ime i prezime: <label> <?php echo $_SESSION['korisnik']['Ime'] . ' ' . $_SESSION['korisnik']['Prezime']; ?></label></div>
                        <div>Adresa: <label> <?php echo $_SESSION['korisnik']['Adresa']; ?></label></div>
                        <div>Kontakt telefon: <label> <?php echo $_SESSION['korisnik']['Mobilni']; ?></label></div>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="panel panel-default">
            <div class="panel-body alert-danger text-center">DA BI STE PORUČILI, MORATE POPUNITI POLJA <B>IME</B> I <B>PREZIME</B> U SVOM PROFILU</div>
        </div>
    <?php endif; ?>

<?php else: ?>
    <div class="panel panel-default">
        <div class="panel-body alert-danger text-center">TRENUTNO NEMATE NI JEDAN PROIZVOD U KOPRI</div>
    </div>
<?php endif; ?>
