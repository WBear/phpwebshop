<?php
//Provera dali postoje parametri za kategoriju i podkategoriju u URL adresi
//Sanitizacija parametara "kategorija" i "podkategorija" pomocu funkcija htmlentities() i strip_tags()
$kategorija = isset($_GET['kategorija']) ? $_GET['kategorija'] : "";
$podkategorija = isset($_GET['podkategorija']) ? $_GET['podkategorija'] : "";

//Instanciranje lista
$proizvodi_lista = [];
$kategorija_lista = [];

//Provera dali je samo glavna kategorija ili i podkategorija kliknuta
if ($podkategorija != "") {
    require_once('./include/skripte/getPodkategorijaProizvodi.php');
    $proizvodi_lista = getPodkategorijaProizvodi($podkategorija);
}

if ($kategorija != "" && array_key_exists($kategorija, $_SESSION['kategorije'])) {
    $podkategorije_lista = $_SESSION['kategorije'][$kategorija];
}

if (sizeof($proizvodi_lista) > 0):
    foreach ($proizvodi_lista as $proizvod):
        ?>
        <div class="col-md-4">
            <div class="panel panel-default proizvod-panel">
                <div class="panel-head text-center">
                    <h4 class="text-fix"><?php echo $proizvod['Naziv']; ?></h4>
                </div>
                <div class="panel-body">
                    <div class="slika-proizvod text-center">
                        <a class="detaljnije" data-toggle="modal" data-target="#proizvodModal" data-id="<?php echo $proizvod['IDProizvoda']; ?>">
                            <img class="cursor" src="./slike_proizvodi/<?php echo $proizvod['Slika']; ?>" width="100%"/>
                        </a>
                    </div>
                    <p class="text-left proizvod-opis"><i class="text-fix2"><?php echo $proizvod['Kratakopis']; ?></i></p>
                    <?php if (!empty($KORISNIK)): ?>
                        <button class="like btn btn-sm btn-success pull-left"
                                data-tip="gore"
                                data-proizvodid="<?php echo $proizvod['IDProizvoda']; ?>"
                                data-korisnik="<?php echo $KORISNIK['IDKorisnika']; ?>"
                                data-brojsvidjanja="<?php echo $proizvod['BrojSvidjanja']; ?>">
                        <span class="glyphicon glyphicon-thumbs-up">
                            (<?php echo $proizvod['BrojSvidjanja']; ?>)</span>
                        </button>
                        <button class="like btn btn-sm btn-danger pull-left"
                                data-tip="dole"
                                data-proizvodid="<?php echo $proizvod['IDProizvoda']; ?>"
                                data-korisnik="<?php echo $KORISNIK['IDKorisnika']; ?>"
                                data-brojsvidjanja="<?php echo $proizvod['BrojNesvidjanja']; ?>">
                        <span class="glyphicon glyphicon-thumbs-down">
                            (<?php echo $proizvod['BrojNesvidjanja']; ?>)</span>
                        </button>
                    <?php else: ?>
                        <button class="btn btn-sm btn-success pull-left" disabled>
                        <span class="glyphicon glyphicon-thumbs-up">
                            (<?php echo $proizvod['BrojSvidjanja']; ?>)</span>
                        </button>
                        <button class="btn btn-sm btn-danger pull-left" disabled>
                        <span class="glyphicon glyphicon-thumbs-down">
                            (<?php echo $proizvod['BrojNesvidjanja']; ?>)</span>
                        </button>
                    <?php endif; ?>
                    <br><br>
                    <label class="pull-right">Cena: <?php echo $proizvod['Cena'] . $VALUTA; ?></label>
                </div>
            </div>
        </div>
        <?php
    endforeach;
elseif (isset($podkategorije_lista) && sizeof($podkategorije_lista) > 0 && $podkategorija == ""):
    foreach ($podkategorije_lista as $podkategorija):
        ?>
        <div class="proizvod">
            <!-- TODO: Ubaciti sliku podkategorije -->
            <p><a href="?strana=proizvodi&kategorija=<?php echo $kategorija; ?>&podkategorija=<?php echo $podkategorija; ?>"><?php echo $podkategorija; ?></a></p>
        </div>
        <?php
    endforeach;
else:
    ?>
    <div class="panel panel-default">
        <div class="panel-body alert-info">Ne postoje proizvodi za izabranu kategoriju / podkategoriju.</div>
    </div>
    <?php
endif;
?>
