<?php

//Dozvoli samo administratorima ovu stranu
if (isset($KORISNIK['administrator']) && $KORISNIK['administrator'] == 1):

//Funckija vraca porudzbine
//Prima parametar isporucene (1) ili ne isporucene (0)
    function Porudzbine($isporuceno)
    {
        global $db;

        //Upit koji vraca sve kategorije iz baze i smesta u listu
        $query_porudzbine = $db->prepare("SELECT *
                                      FROM `kupovine` `ku`
                                      LEFT JOIN `korisnici` `k`
                                        ON `k`.`IDKorisnika` = `ku`.`Korisnik`
                                      WHERE
                                      (? = 1 AND `ku`.`Isporuceno` IS NOT NULL)
                                      OR
                                      (? = 0 AND `ku`.`Isporuceno` IS NULL)
                                      ORDER BY `ku`.`IDKupovine` DESC");
        $query_porudzbine->execute(array($isporuceno, $isporuceno));
        $porudzbine_lista = $query_porudzbine->fetchALL(PDO::FETCH_ASSOC);
        $query_porudzbine->closecursor();

        return $porudzbine_lista;
    }

    $neIsporucenePorudzbine = Porudzbine(0);
    $isporucenPorudzbine = porudzbine(1);


    ?>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#na-cekanju">Na čekanju</a></li>
        <li><a data-toggle="tab" href="#isporucene">Isporučene</a></li>
    </ul>

    <div class="tab-content">
        <div id="na-cekanju" class="tab-pane fade in active">
            <BR>
            <div class="row">
                <div class="col-md-12">
                    <?php if (sizeof($neIsporucenePorudzbine) > 0): ?>
                        <?php foreach ($neIsporucenePorudzbine as $porudzbina):
                            $porudzbinaPodaci = json_decode($porudzbina['Kupljeniproizvodi'], true);
                            $ukupna_cena = 0; ?>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table class="korpa-proizvodi">
                                        <thead>
                                        <div>
                                            <label>ID Porudžbine: <?php echo $porudzbina['IDKupovine']; ?></label>
                                        </div>
                                        <div>
                                            ID Korisnika: <?php echo $porudzbina['IDKorisnika']; ?> | Ime i prezime: <?php echo $porudzbina['Ime'] . ' ' . $porudzbina['Prezime']; ?> <BR>
                                            Adresa: <?php echo $porudzbinaPodaci['korisnik']['adresa']; ?> | Kontakt telefon: <?php echo $porudzbinaPodaci['korisnik']['mobilni']; ?>
                                        </div>
                                        <HR>
                                        <tr>
                                            <td class="text-left"><label>ID</label></td>
                                            <td class="text-left"><label>Naziv</label></td>
                                            <td class="text-right"><label>Jedinična cena</label></td>
                                            <td class="text-right"><label>Količina</label></td>
                                            <td class="text-right"><label>Ukupno</label></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($porudzbinaPodaci as $index => $proizvod):
                                            if ($index != "korisnik"): ?>
                                                <tr>
                                                    <td class="text-left"><?php echo $index; ?></td>
                                                    <td class="text-left"><?php echo $proizvod['naziv']; ?></td>
                                                    <td class="text-right"><?php echo $proizvod['cena'] . ' ' . $VALUTA; ?></td>
                                                    <td class="text-right"><?php echo $proizvod['kolicina']; ?></td>
                                                    <td class="text-right"><?php echo $proizvod['cena'] * $proizvod['kolicina'] . ' ' . $VALUTA; ?></td>
                                                    <?php $ukupna_cena = $ukupna_cena + $proizvod['cena'] * $proizvod['kolicina']; ?>
                                                </tr>
                                            <?php endif;
                                        endforeach; ?>
                                        <tr>
                                            <td class="text-right" colspan="4"><label>SVE UKUPNO</label></td>
                                            <td class="suma-korpa text-right"><label><?php echo $ukupna_cena . ' ' . $VALUTA; ?></label></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <p class="pull-left">
                                        <button class="btn btn-danger" onclick="administratorPorudzbine(<?php echo $porudzbina['IDKupovine']; ?>, 'obrisi');">Obriši</button>
                                    </p>
                                    <p class="pull-right">
                                        <button class="btn btn-success" onclick="administratorPorudzbine(<?php echo $porudzbina['IDKupovine']; ?>, 'isporuci');">Isporuči</button>
                                    </p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="panel panel-default">
                            <div class="panel-body alert-info text-center">TRENUTNO NEMA PORUDŽBINA NA ČEKANJU</div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div id="isporucene" class="tab-pane fade">
            <BR>
            <div class="row">
                <div class="col-md-12">
                    <?php if (sizeof($isporucenPorudzbine) > 0): ?>
                        <?php foreach ($isporucenPorudzbine as $porudzbina):
                            $porudzbinaPodaci = json_decode($porudzbina['Kupljeniproizvodi'], true);
                            $ukupna_cena = 0; ?>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table class="korpa-proizvodi">
                                        <thead>
                                        <div>
                                            <label>ID Porudžbine: <?php echo $porudzbina['IDKupovine']; ?> | Datum
                                                isporuke: <?php echo date('d.m.Y / H:m', strtotime($porudzbina['Isporuceno'])); ?></label>
                                        </div>
                                        <div>
                                            ID Korisnika: <?php echo $porudzbina['IDKorisnika']; ?> | Ime i prezime: <?php echo $porudzbina['Ime'] . ' ' . $porudzbina['Prezime']; ?> <BR>
                                            Adresa: <?php echo $porudzbinaPodaci['korisnik']['adresa']; ?> | Kontakt telefon: <?php echo $porudzbinaPodaci['korisnik']['mobilni']; ?>
                                        </div>
                                        <HR>
                                        <tr>
                                            <td class="text-left"><label>ID</label></td>
                                            <td class="text-left"><label>Naziv</label></td>
                                            <td class="text-right"><label>Jedinična cena</label></td>
                                            <td class="text-right"><label>Količina</label></td>
                                            <td class="text-right"><label>Ukupno</label></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($porudzbinaPodaci as $index => $proizvod):
                                            if ($index != "korisnik"): ?>
                                                <tr>
                                                    <td class="text-left"><?php echo $index; ?></td>
                                                    <td class="text-left"><?php echo $proizvod['naziv']; ?></td>
                                                    <td class="text-right"><?php echo $proizvod['cena'] . ' ' . $VALUTA; ?></td>
                                                    <td class="text-right"><?php echo $proizvod['kolicina']; ?></td>
                                                    <td class="text-right"><?php echo $proizvod['cena'] * $proizvod['kolicina'] . ' ' . $VALUTA; ?></td>
                                                    <?php $ukupna_cena = $ukupna_cena + $proizvod['cena'] * $proizvod['kolicina']; ?>
                                                </tr>
                                            <?php endif;
                                        endforeach; ?>
                                        <tr>
                                            <td class="text-right" colspan="4"><label>SVE UKUPNO</label></td>
                                            <td class="suma-korpa text-right"><label><?php echo $ukupna_cena . ' ' . $VALUTA; ?></label></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="panel panel-default">
                            <div class="panel-body alert-info text-center">TRENUTNO NEMA ISPORUČENIH PORUDŽBINA</div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php else: ?>
    <!-- Poruka da proizvod pod tim imenom vec postoji u bazi -->
    <div class="panel panel-default">
        <div class="panel-body alert-danger text-center">OVA STRANICA JE NAMENJENA ADMINISTRATORIMA.</div>
    </div>
<?php endif; ?>