<?php if ($_SERVER['REQUEST_METHOD'] == 'POST'): ?>
    <!-- Poruka o uspesnom slanju --FAKE -->
    <div class="panel panel-default">
        <div class="panel-body alert-success text-center">Uspešno ste poslali komentar.</div>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <label>Internet prodavnica, d.o.o.</label></br>
                Adresa:<label>Kneza Miloša 1a, Beograd</label></br>
                Telefon:<label>011/222-333</label></br>

                <!-- Google mapa -->
                <p>
                    <?php include('./include/layout/googleMapa.php'); ?>
                </p>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body ">
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="text">Kontaktirajte nas</label>
                        <textarea rows="10" class="form-control" type="text" name="text"></textarea>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default" type="submit" name="submit" value="1">Pošalji</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>