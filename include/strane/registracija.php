<?php

//Provera dali je forma prosledjena i dali je uspesna registracija
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    include('./include/skripte/registracija.php');

    //Provera dali je uspesna registracija
    if (registracija()): ?>
        <!-- Poruka o uspesnoj registraciji-->
        <div class="panel panel-default">
            <div class="panel-body alert-success text-center">Uspešno ste se registrovali.</div>
        </div>

        <!-- Automatski redirektuj korisnika na pocetnu stranu posle 2 sekunde-->
        <script type="text/javascript">
            setTimeout(function() {
                window.location.href = "./";
            }, 1500);
        </script>

    <?php else: ?>
        <!-- Poruka da korisnicko ime vec postoji-->
        <div class="panel panel-default">
            <div class="panel-body alert-danger text-center">Korinik sa izabranim korisničkim imenom već postoji u bazi.</div>
        </div>
        <?php
        /* Prikazi ponovo registracionu formu*/
        include('./include/layout/_formaRegistracija.php');
    endif;

} else {
    /* Forma nije prosledjena, prikazi registracionu formu*/
    include('./include/layout/_formaRegistracija.php');
}